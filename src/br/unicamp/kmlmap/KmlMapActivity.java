package br.unicamp.kmlmap;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.maps.MapController;
import com.google.android.maps.Overlay;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import android.graphics.drawable.Drawable;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Iterator;

import br.unicamp.kmlmap.kml.KmlRoot;
import br.unicamp.kmlmap.kml.Placemark;

public class KmlMapActivity extends MapActivity
{
    List<Overlay> mapOverlays;
    Drawable drawable;
    MapKmlItemizedOverlay itemizedoverlay;

    List<GeoPoint> placemarks;

    private static final String LOG_TAG_E = "KML_MAP_ERROR";
    private static final String LOG_TAG_D = "KML_MAP_DEBUG";

    private static final String KML_ASSET_NAME = "sample.kml";

    private MapView map;
    private MapController controller;
    GeoPoint point;
    int i;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        map = (MapView) findViewById(R.id.map);
        map.setBuiltInZoomControls(true);

        mapOverlays = map.getOverlays();
        drawable = this.getResources().getDrawable(R.drawable.androidmarker);
        itemizedoverlay = new MapKmlItemizedOverlay(drawable);

        try {
            placemarks = getKmlPlacemarks(KML_ASSET_NAME);
        } catch (Exception e) {
            Log.e(LOG_TAG_E, e.getMessage());
        }

        for (i = 0; i < placemarks.size(); i++) {
            OverlayItem overlayitem = new OverlayItem(placemarks.get(i), "", "");

            itemizedoverlay.addOverlay(overlayitem);
            mapOverlays.add(itemizedoverlay);
	    }

        controller = map.getController();
        controller.setCenter(placemarks.get(i-1));
        controller.animateTo(placemarks.get(i-1));
        controller.setZoom(16);
    }

    @Override
    protected boolean isRouteDisplayed() {
        // TODO stub!
        return false;
    }

    /**
     * Parses a kml file and returns the coordinates of its placemarks as a list of GeoPoints
     * suitable for displaying in a map overlay.
     * @param kmlFile The name of the kml file, in the assets directory.
     * @return A list of GeoPoints, one for each placemark.
     */
    private List<GeoPoint> getKmlPlacemarks(String kmlFile) throws Exception {
        List<GeoPoint> placemarks = new ArrayList<GeoPoint>();
        InputStream is = getAssets().open(kmlFile);
        Serializer serializer = new Persister();
        KmlRoot kml = serializer.read(KmlRoot.class, is);

        for (Placemark p : kml.getDocument().getPlacemarks()) {
            placemarks.add(getGeoPointFromCoordinate(p.getCoordinates()));
            Log.d(LOG_TAG_D, p.getName());
        }

        return placemarks;
    }

    /**
     * Converts a coordinate string into a GeoPoint.
     * @param coordinate The coordinate string, in the form "longitude, latitude"
     * @return A GeoPoint with the given coordinates.
     */
    private GeoPoint getGeoPointFromCoordinate(String coordinate) {
        StringTokenizer st = new StringTokenizer(coordinate, ",");
        float lo = Float.parseFloat(st.nextToken());
        float la = Float.parseFloat(st.nextToken());
        return new GeoPoint((int) (la * 1E6), (int) (lo * 1E6));
    }
}
